package thyroid

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.ml.classification.DecisionTreeClassifier
import org.apache.spark.rdd._
import org.apache.spark.mllib.linalg.{DenseVector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint

import scala.collection.mutable
import scala.math._
import org.apache.spark.mllib.stat.{MultivariateStatisticalSummary, Statistics}
import thyroid.MetricDistance.MetricDistance
import java.io.{File, PrintWriter}


object SparkThyroid extends App{

  override def main(args: Array[String]) {

  val localFilePrefix = "file://"
  //val homePath= "/home/francine"
  val homePath = "/home/Francine/Dataset/"
  val defaultFilePath = homePath + "ann-train.data"
  //val defaultTestPath= localFilePrefix + homePath + "ann-test.data"


  //Set Memory Usage and overwriting
  val conf = new SparkConf().setAppName("SparkThyroid").set("spark.executor.memory", "4g").set("spark.hadoop.validateOutputSpecs", "false")
  val sc = new SparkContext(conf)


  //_______PARSING

  val usage =
    """
    Usage: [-k num] [-m euclidean | gower] [-outputPath path_to_output] [-in path_to_input]  [-s (itemtoParse in CSV)]
    """
  //if (args.length == 0) println(usage)
  val arglist = args.toList
  type OptionMap = Map[Symbol, Any]

  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    def isSwitch(s: String) = (s(0) == '-')
    list match {
      case Nil => map
      case "-k" :: value :: tail =>
        nextOption(map ++ Map('k -> value.toInt), tail)
      case "-m" :: value :: tail =>
        nextOption(map ++ Map('metric -> value), tail)
      case "-s" :: value :: tail =>
        nextOption(map ++ Map('item -> value), tail)
      case "-in" :: value :: tail =>
        nextOption(map ++ Map('inputPath  -> value), tail)
      case "-outputPath" :: value :: tail =>
        nextOption(map ++ Map('outputPath -> value), tail)
      //case string :: opt2 :: tail if isSwitch(opt2) =>
      // nextOption(map ++ Map('input_file -> string), list.tail)
      case string :: Nil =>  nextOption(map ++ Map('input_file -> string), list.tail)
      case option :: tail => println("Unknown option " + option); Map()
    }
  }

  val options = nextOption(Map(), arglist)
  println(options)

  val k_from_args: Int = options.get('k) match {
    case Some(s: Int) => s
    case _ => 5
  }

  val outputfromArgs: String = options.get('outputPath) match {
    case Some(s: String) => localFilePrefix + s.toString
    case _ => localFilePrefix + homePath + "result"
  }

    val inputfromArgs: Option[String] = options.get('inputPath) match {
      case Some(s: String) => Some(s.toString)
      case _ => None
    }


  val metricfromArgs: String = options.get('metric) match {
    case Some(s: String) => s
    case _ => "euclidean" // if no metric was correctly parsed, the default is Euclidean
  }

  val metricToBeUsed = Utils.parseDistance(metricfromArgs)
  if (metricToBeUsed == MetricDistance.EUCLIDEAN) println("Using Euclidean Distance")
  if (metricToBeUsed == MetricDistance.GOWER) println("Using Gower Dissimilarity as Distance")

  val itemfromArgs: String = options.get('item) match {
    case Some(s: String) => s.toString
    case _ => ""
  }

  if (itemfromArgs.isEmpty || inputfromArgs.isEmpty) {
    println("Item not parsed well from CLI or no input specified")
    println(usage)
    sc.stop()
  }

  else {

    def parseInputDir(path: String): String = {
      val indexLastSlash= path.lastIndexOf('/')
      path.substring(0,indexLastSlash+1)
    }

    println(s"record is $itemfromArgs")

    val fullpath= localFilePrefix+ inputfromArgs.get
    val dataset = sc.textFile(fullpath).map(x => Utils.parseFromStringLabel(x))

    val record2check = Utils.parseFromStringVector(itemfromArgs)

    if (metricToBeUsed == MetricDistance.EUCLIDEAN) {
      val similarities = dataset.map(x => (math.sqrt(Vectors.sqdist(x.features, record2check)), x.label)).takeOrdered(k_from_args)
      sc.makeRDD(similarities).repartition(1).map(x=> s"${x._1},${x._2}").saveAsTextFile(outputfromArgs)
    }

    if (metricToBeUsed == MetricDistance.GOWER) {

      val currentPath=parseInputDir(inputfromArgs.get)
      val rangeFile= new File(currentPath+"range_thyroid.csv")
      val rangeVectorFromFile: Option[DenseVector]= if(rangeFile.exists()){
        val path= localFilePrefix+rangeFile.getAbsolutePath
        val vectData: Array[Double] = sc.textFile(path).map(x => BigDecimal(x).toDouble).collect()
        val vect: DenseVector = new DenseVector(vectData)
        Some(vect)
      } else None
      //Necessary for Gower
      val rangeVector =
        if (rangeVectorFromFile.isDefined) rangeVectorFromFile.get
        else {
          val obs = Statistics.colStats(dataset.map(_.features))
          val maxValues = obs.max
          val minValues = obs.min
          val range = (for (index <- 0 to maxValues.size - 1) yield maxValues(index) - minValues(index)).toArray
          new DenseVector(range)
        }
      val sim = dataset.map(x => (Utils.gowenDissimilarity(x.features.toDense, record2check, rangeVector), x.label)).sortBy(_._1).take(k_from_args)
      sc.makeRDD(sim).repartition(1).map(x=> (s"${x._1},${x._2}")).
        saveAsTextFile(outputfromArgs)
      if(rangeVectorFromFile.isEmpty){
        val arrA= rangeVector.values.map(x => f"$x%e")
        sc.makeRDD(arrA).repartition(1).saveAsTextFile(localFilePrefix+rangeFile.getAbsolutePath)
        }
    }

    //To print it nice
    // val df = new java.text.DecimalFormat("##.####")
    // df.format(y)

    sc.stop()
  }
  }
}