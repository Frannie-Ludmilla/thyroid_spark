package thyroid

import org.apache.spark.mllib.linalg.{DenseVector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import thyroid.MetricDistance._

object Utils{
  def parseFromStringLabel(item: String): LabeledPoint = {
    val arrDouble = item.split(" ").map(_.toDouble)
    new LabeledPoint(arrDouble(21), Vectors.dense(arrDouble.slice(0, 21)))
  }

  def parseFromStringVector(item: String): DenseVector = {
    new DenseVector(item.split(",").map(_.toDouble))
  }

  def gowenDissimilarity(v1: DenseVector, v2: DenseVector, r: DenseVector): Double = {
    var result, gowenCoeff = 0.0
    if (v1.size == v2.size) {
      var index = 0
      gowenCoeff = 0.0
      while (index < v1.size) {
        gowenCoeff += math.abs(v1(index) - v2(index)) / r(index)
        index += 1
      }
      //result= math.sqrt((1.0/r.toArray.sum)*gowenCoeff)
      result = gowenCoeff / v1.size
    }
    else result = -1
    result
  }

  def parseDistance(str: String): MetricDistance = str match {
    case s if s matches "(?i)euclidean" => MetricDistance.EUCLIDEAN
    case s if s matches "(?i)gower" => MetricDistance.GOWER
    case _ => MetricDistance.EUCLIDEAN
  }
}