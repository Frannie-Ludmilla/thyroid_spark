package thyroid

/**
  * Created by Francine on 4/21/16.
  */
object MetricDistance extends Enumeration {
  type MetricDistance = Value
  val GOWER, EUCLIDEAN = Value
}
