#!/usr/bin/env bash
FILENAME="/home/Francine/Dataset/Thyroid/ann-test.data"
INPUT="/home/Francine/Dataset/Thyroid/ann-train_90.data"
rm -r record.txt
#shuf retrieves a random line, awk cuts out the last column, sed changes the whitespaces for commas
shuf -n 1 < ${FILENAME} |awk 'NF{NF--};1'| sed -e "s/ /,/g" >> record.txt
RECORD=`cat record.txt`
spark-submit ./target/scala-2.11/sparkthyroid_2.11-1.0.jar -m gower -s ${RECORD} -k 3 -in ${INPUT} -outputPath /home/Francine/gowen
