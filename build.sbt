name := "SparkThyroid"
version := "1.0"
scalaVersion := "2.11.7"

libraryDependencies ++= {
  lazy val sparkVersion= "1.6.0"
  Seq(
    "org.apache.spark" %% "spark-core" % sparkVersion % Provided,
    "org.apache.spark" %%  "spark-mllib" % sparkVersion % Provided
  )
}

//mainClass in Compile := Some("thyroid.SparkThyroid")

lazy val root = (project in file("."))